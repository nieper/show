;; Copyright (C) Marc Nieper-Wißkirchen (2020).  All Rights Reserved.

;; Permission is hereby granted, free of charge, to any person
;; obtaining a copy of this software and associated documentation
;; files (the "Software"), to deal in the Software without
;; restriction, including without limitation the rights to use, copy,
;; modify, merge, publish, distribute, sublicense, and/or sell copies
;; of the Software, and to permit persons to whom the Software is
;; furnished to do so, subject to the following conditions:

;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;; NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
;; BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
;; ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
;; CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
;; SOFTWARE.


(define-library (srfi 166 columnar)
  (export columnar tabular wrapped wrapped/list wrapped/char justified from-file
	  line-numbers)
  (import (scheme base)
	  (scheme case-lambda)
	  (scheme file)
	  (srfi 1)
	  (srfi 8)
	  (srfi 133)
	  (srfi 158)
	  (rename (srfi 165)
		  (computation-bind bind)
		  (computation-bind/forked bind/forked)
		  (computation-environment-copy environment-copy)
		  (computation-local local)
		  (computation-pure return)
		  (computation-sequence sequence))
	  (srfi 166 base))
  (include "word-wrap.scm"
	   "columnar.scm"))
