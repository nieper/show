;; Copyright (C) Marc Nieper-Wißkirchen (2020).  All Rights Reserved.

;; Permission is hereby granted, free of charge, to any person
;; obtaining a copy of this software and associated documentation
;; files (the "Software"), to deal in the Software without
;; restriction, including without limitation the rights to use, copy,
;; modify, merge, publish, distribute, sublicense, and/or sell copies
;; of the Software, and to permit persons to whom the Software is
;; furnished to do so, subject to the following conditions:

;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;; NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
;; BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
;; ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
;; CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
;; SOFTWARE.

(define (as-unicode . fmt*)
  (fn (ambiguous-is-wide?)
    (with ((string-width (if ambiguous-is-wide?
			     unicode-terminal-width/wide
			     unicode-terminal-width)))
      (each-in-list fmt*))))

;;; FIXME: Implement ANSI escape sequences.
(define (%unicode-terminal-width str wide?)
  (let loop ((chars (string->list str)) (width 0))
    (cond ((null? chars) width)
	  ((and (not (null? (cdr chars)))
		(char=? (car chars) #\x1b)
		(char=? (cadr chars) #\[))
	   (let loop/escape ((chars (cddr chars)))
	     (cond ((null? chars) width)
		   ((char=? (car chars) #\m) (loop (cdr chars) width))
		   (else (loop/escape (cdr chars))))))
	  (else
	   (loop (cdr chars) (+ width
				(max 0 (uc-width (char->integer (car chars))
						 wide?))))))))

(define (unicode-terminal-width str)
  (%unicode-terminal-width str #f))

(define (unicode-terminal-width/wide str)
  (%unicode-terminal-width str #t))
